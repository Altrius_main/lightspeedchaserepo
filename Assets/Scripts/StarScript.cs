﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles a single background star's movement
/// </summary>
public class StarScript : MonoBehaviour
{
    // References to the star's elements
    private Rigidbody rigbody;
    private Transform transf;
    private SpriteRenderer spriterend;

    // Variables
    private float distance; // How distant the individual star is
    private const float maxDistance = 100; // Constant to describe the maximium possible distance of the star
    private float speed; // How fast the individual star is moving

    private void Start()
    {
        // Get references to the star's elements
        rigbody = GetComponent<Rigidbody>();
        transf = GetComponent<Transform>();
        spriterend = GetComponent<SpriteRenderer>();

        // Randomize the star's initial position
        transf.position = new Vector2(Random.Range(-Screen.width/10, Screen.width/10), Random.Range(-Screen.height/10, Screen.height/10));
        Initialize(); // Initialise the star
    }

    private void Update()
    {
        if (transf.position.x < -Screen.width/10) Reset(); // Reset the star if it has moved out of the map
    }

    private void Reset()
    {   // Rnadomizes the star's new initial position
        transf.position = new Vector2(Screen.width/10, Random.Range(-Screen.height/10, Screen.height/10)); // TODO: adjust scene so that Scene.height and width describe the scene by themselves
        Initialize(); // Initialise the star
    }

    /// <summary>
    /// Lauch star to movement
    /// </summary>
    private void Initialize()
    {
        distance = Random.Range(Random.Range(Random.Range(1, maxDistance), maxDistance), maxDistance); // The amount of stars increases with the distance
        float sqdistance = Mathf.Pow(distance, 2); // Precalculate the square of the distance

        speed = (600000f / sqdistance); // Set the star's speed according to it's distance
        transf.localScale = new Vector2(20000f / sqdistance, 0.1f); // Set the star's size according to it's distance
        spriterend.color = new Color(1f, 1f, 1f, (0.75f - 0.006f * distance)); // Set the star's fading according to distance

        rigbody.velocity = new Vector2(-speed, 0); // This is what makes the star to actually move
    }
}