﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lightspeed
{

    public class StarSpawner : MonoBehaviour
    {
        public GameObject star; // Star prefab to be set in unity editor
        private int starAmount = 2000; // The amount of stars in scene

        // Start is called before the first frame update
        void Start()
        {
            // Spawn set amount of stars to the scene
            for (int i = 0; i < starAmount; i++)
            {
                Instantiate(star); // Spawns a single star
            }
        }
        
    }

}