# Light Speed Chase

A 2-player space ship dogfight game of mine.

## Getting Started

Built version of the game can be launched from the LatestBuild folder.
To modify the game you need Unity game engine of version 2020.3.0f1.

### Prerequisites

To run and modify the project, you will need Unity game engine with c# editor. The project can be opened from your local repository.

## Authors

* [Joonas Pollari](https://github.com/JNPollari)


## Acknowledgments

The project's soundscape was possible thanks to [OpenGameArt](https://opengameart.org/) contributions of:

* Ville Nousiainen (http://soundcloud.com/mutkanto)
* Dan Knoflicek
* Skorpio
* bart
* NenadSimic
* CosmicD
* Kenney
* Little Robot Sound Factory (www.littlerobotsoundfactory.com)
